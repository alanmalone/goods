<?php
/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 14.03.2016
 * Time: 21:00
 */

return array(
    'report/volumes' => 'report/volumes',
    'report/deliveries' => 'report/deliveries',
    'report' => 'report/index',
    'invoice/add' => 'invoice/add',
    'invoice/detail/([0-9]+)' => 'invoice/detail/$1',
    'invoice/delivery/([0-9]+)' => 'invoice/delivery/$1',
    'invoice' => 'invoice/index',
    'suppl/edit/([0-9]+)' => 'suppl/edit/$1',
    'suppl/delete/([0-9]+)' => 'suppl/delete/$1',
    'suppl/add' => 'suppl/add',
    'suppl' => 'suppl/index',
    'goods/edit/([0-9]+)' => 'goods/edit/$1',
    'goods/delete/([0-9]+)' => 'goods/delete/$1',
    'goods/add' => 'goods/add',
    'goods' => 'goods/index',
    'index.php' => 'site/index',
    '' => 'site/index',
);