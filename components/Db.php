<?php

/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 10.04.2016
 * Time: 23:00
 */
class Db {
    public static function getConnection() {
        $params_path = ROOT.'/config/db_params.php';
        $params = include ($params_path);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);

        $db->exec("set names utf8");

        return $db;
    }
}