<?php

/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 14.06.2016
 * Time: 0:38
 */
class Detail {
    public static function getDetailById($num_i) {
        $db = Db::getConnection();

        $sql = "SELECT * FROM inv_g WHERE num_i = :num_i";

        $result = $db->prepare($sql);
        $result->bindParam(":num_i", $num_i, PDO::PARAM_STR);

        $result->execute();

        return $result->fetch();
    }

    public static function getDetailBySupplId($code_s) {
        $db = Db::getConnection();

        $sql = "SELECT name_g, invsupl.qt, invsupl.price_p FROM goods INNER JOIN (SELECT * FROM inv_g WHERE code_s = :code_s) AS invsupl ON goods.art = invsupl.art";

        $result = $db->prepare($sql);
        $result->bindParam(":code_s", $code_s, PDO::PARAM_STR);

        $result->execute();

        $i = 0;
        $detailInvoice = array();

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $detailInvoice[$i]['name_g'] = $row['name_g'];
            $detailInvoice[$i]['qt'] = $row['qt'];
            $detailInvoice[$i]['price_p'] = $row['price_p'];
            $i++;
        }

        return $detailInvoice;
    }
}