<?php

/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 11.05.2016
 * Time: 16:57
 */
class Suppl {
    public static function getAllSuppls() {
        $db = Db::getConnection();


        $result = $db->query('SELECT * FROM suppl');

        $i = 0;
        $supplsList = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $supplsList[$i]['code_s'] = $row['code_s'];
            $supplsList[$i]['name_s'] = $row['name_s'];
            $supplsList[$i]['city_s'] = $row['city_s'];
            $supplsList[$i]['phone_s'] = $row['phone_s'];
            $supplsList[$i]['info_s'] = $row['info_s'];
            $i++;
        }

        return $supplsList;
    }

    public static function addSuppl($code_s, $name_s, $city_s, $phone_s, $stat, $info_s) {
        $db = Db::getConnection();

        $sql = "INSERT INTO suppl(code_s, name_s, city_s, phone_s, stat, info_s) VALUES (:code_s, :name_s, :city_s, :phone_s, :stat, :info_s)";

        $result = $db->prepare($sql);

        $result->bindParam(':code_s', $code_s, PDO::PARAM_STR);
        $result->bindParam(':name_s', $name_s, PDO::PARAM_STR);
        $result->bindParam(':city_s', $city_s, PDO::PARAM_STR);
        $result->bindParam(':phone_s', $phone_s, PDO::PARAM_STR);
        $result->bindParam(':stat', $stat, PDO::PARAM_STR);
        $result->bindParam(':info_s', $info_s, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function deleteSupplById($code_s) {
        $db = Db::getConnection();

        $sql = "DELETE FROM suppl WHERE code_s = :code_s";

        $result = $db->prepare($sql);

        $result->bindParam(":code_s", $code_s, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function getSupplById($code_s) {
        $db = Db::getConnection();

        $sql = "SELECT * FROM suppl WHERE code_s = :code_s";

        $result = $db->prepare($sql);

        $result->bindParam(":code_s", $code_s, PDO::PARAM_STR);

        $result->execute();

        return $result->fetch();
    }

    public static function updateSupplById($code_s, $name_s, $city_s, $phone_s, $stat, $info_s) {
        $db = Db::getConnection();

        $sql = "UPDATE suppl SET name_s = :name_s, city_s = :city_s, phone_s = :phone_s, stat = :stat, info_s = :info_s WHERE code_s = :code_s";

        $result = $db->prepare($sql);

        $result->bindParam(':code_s', $code_s, PDO::PARAM_STR);
        $result->bindParam(':name_s', $name_s, PDO::PARAM_STR);
        $result->bindParam(':city_s', $city_s, PDO::PARAM_STR);
        $result->bindParam(':phone_s', $phone_s, PDO::PARAM_STR);
        $result->bindParam(':stat', $stat, PDO::PARAM_STR);
        $result->bindParam(':info_s', $info_s, PDO::PARAM_STR);

        return $result->execute();
    }
}