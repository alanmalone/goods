<?php

/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 13.06.2016
 * Time: 16:50
 */
class Invoice {
    public static function getInvoiceBySupplId($code_s) {
        $db = Db::getConnection();

        $sql = "SELECT * FROM invoice WHERE code_s = :code_s";

        $result = $db->prepare($sql);
        $result->bindParam(":code_s", $code_s, PDO::PARAM_STR);

        $result->execute();

        $i = 0;
        $invoicesBySupple = array();

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $invoicesBySupple[$i]['num_i'] = $row['num_i'];
            $invoicesBySupple[$i]['code_s'] = $row['code_s'];
            $invoicesBySupple[$i]['date_p'] = $row['date_p'];
            $i++;
        }

        return $invoicesBySupple;
    }

    public static function addInvoice($num_i, $code_s, $date_p, $art, $qt, $price_p) {
        $db = Db::getConnection();

        $sqlInvoice = "INSERT INTO invoice(num_i, code_s, date_p) VALUES (:num_i, :code_s, :date_p)";
        $sqlInv_g = "INSERT INTO inv_g(num_i, code_s, art, qt, price_p) VALUES (:num_i, :code_s, :art, :qt, :price_p)";

        $resultInvoice = $db->prepare($sqlInvoice);
        $resultInvoice->bindParam(":num_i", $num_i, PDO::PARAM_STR);
        $resultInvoice->bindParam(":code_s", $code_s, PDO::PARAM_STR);
        $resultInvoice->bindParam(":date_p", $date_p, PDO::PARAM_STR);
        $resultInvoice->execute();

        $resultInv_g = $db->prepare($sqlInv_g);
        $resultInv_g->bindParam(":num_i", $num_i, PDO::PARAM_STR);
        $resultInv_g->bindParam(":code_s", $code_s, PDO::PARAM_STR);
        $resultInv_g->bindParam(":art", $art, PDO::PARAM_STR);
        $resultInv_g->bindParam(":qt", $qt, PDO::PARAM_INT);
        $resultInv_g->bindParam(":price_p", $price_p, PDO::PARAM_INT);
        $resultInv_g->execute();
    }

    public static function getInvoiceSupplies() {
        $db = Db::getConnection();

        $sql = "SELECT suppl.name_s, suppl.code_s FROM suppl INNER JOIN (SELECT * FROM `inv_g` GROUP BY `code_s`) AS sup ON sup.code_s = suppl.code_s";

        $result = $db->prepare($sql);

        $result->execute();

        $i = 0;
        $invoiceSupplies = array();

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $invoiceSupplies[$i]['code_s'] = $row['code_s'];
            $invoiceSupplies[$i]['name_s'] = $row['name_s'];
            $i++;
        }

        return $invoiceSupplies;
    }

    public static function getSupplByInvoiceGood($art) {
        $db = Db::getConnection();

        $sql = "SELECT name_s, good.price_p FROM suppl INNER JOIN (SELECT * FROM `inv_g` WHERE art = :art) AS good ON good.code_s = suppl.code_s";

        $result = $db->prepare($sql);
        $result->bindParam(':art', $art, PDO::PARAM_STR);

        $result->execute();

        $i = 0;
        $suppliesByGood = array();

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $suppliesByGood[$i]['name_s'] = $row['name_s'];
            $suppliesByGood[$i]['price_p'] = $row['price_p'];
            $i++;
        }

        return $suppliesByGood;
    }
}