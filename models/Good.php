<?php

/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 11.04.2016
 * Time: 15:41
 */
class Good {
    public static function getAllGoods() {

        $db = Db::getConnection();


        $result = $db->query('SELECT * FROM goods');

        $i = 0;
        $goodsList = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $goodsList[$i]['art'] = $row['art'];
            $goodsList[$i]['name_g'] = $row['name_g'];
            $goodsList[$i]['meas_g'] = $row['meas_g'];
            $i++;
        }

        return $goodsList;
    }

    public static function addGood($vendorCode, $nameG, $measG) {

        $db = Db::getConnection();

        $sql = "INSERT INTO goods(art, name_g, meas_g) VALUES (:vendorCode, :nameG, :measG)";

        $result = $db->prepare($sql);
        $result->bindParam(':vendorCode', $vendorCode, PDO::PARAM_STR);
        $result->bindParam(':nameG', $nameG, PDO::PARAM_STR);
        $result->bindParam(':measG', $measG, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function deleteGoodById($id) {

        $db = Db::getConnection();

        $sql = "DELETE FROM goods WHERE art = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function getGoodById($id) {

        $db = Db::getConnection();

        $sql = "SELECT * FROM goods WHERE art = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_STR);

        $result->execute();

        return $result->fetch();
    }

    public static function updateGood($art, $name_g, $meas_g) {

        $db = Db::getConnection();

        $sql = "UPDATE goods SET name_g = :name_g, meas_g = :meas_g WHERE art = :art";

        $result = $db->prepare($sql);
        $result->bindParam(':name_g', $name_g, PDO::PARAM_STR);
        $result->bindParam(':meas_g', $meas_g, PDO::PARAM_STR);
        $result->bindParam(':art', $art, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function getInvoiceGoods() {
        $db = Db::getConnection();

        $sql = "SELECT name_g, goods.art FROM goods INNER JOIN (SELECT * FROM inv_g GROUP BY art) AS invsupl ON goods.art = invsupl.art";

        $result = $db->prepare($sql);
        $result->execute();

        $i = 0;
        $invoiceGoods = array();

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $invoiceGoods[$i]['name_g'] = $row['name_g'];
            $invoiceGoods[$i]['art'] = $row['art'];
            $i++;
        }

        return $invoiceGoods;
    }
}