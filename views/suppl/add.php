<? include ROOT.'/views/layout/header.php' ?>

<main>
    <form action="" method="post">
        <div class="container">
            <form class="col s12" style="margin-top: 10px">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="codeS" name="code_s" type="text" class="validate" maxlength="4">
                        <label for="codeS">Код поставщика</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="nameS" name="name_s" type="text" class="validate" maxlength="100">
                        <label for="nameS">Название поставщика</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="cityS" name="city_s" type="text" class="validate" maxlength="20">
                        <label for="cityS">Город поставщика</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="phoneS" name="phone_s" type="text" class="validate" maxlength="11">
                        <label for="phoneS">Телефон поставщика</label>
                    </div>
                </div>
                <label>
                    <select class="browser-default" name="stat">
                        <option value="" disabled selected>Надежность поставщика</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="50">50</option>
                        <option value="60">60</option>
                        <option value="70">70</option>
                        <option value="80">80</option>
                        <option value="90">90</option>
                        <option value="100">100</option>
                    </select>
                </label>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="infoS" name="info_s" type="text" class="validate" maxlength="128">
                        <label for="infoS">Дополнительная информация</label>
                    </div>
                </div>
                <div class="center-align" style="margin-top: 10px">
                    <button class="btn waves-effect waves-light" type="submit" name="addSuppl">
                        Добавить<i class="material-icons right">add</i>
                    </button>
                </div>
            </form>
        </div>
    </form>
</main>

<?include ROOT.'/views/layout/footer.php' ?>
