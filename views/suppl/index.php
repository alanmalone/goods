<? include ROOT.'/views/layout/header.php' ?>
<main>
    <div class="container">
        <div class="center-align" style="margin-top: 10px">
            <a href="suppl/add">
                <button class="btn waves-effect waves-light" type="submit" name="action">
                    Добавить<i class="material-icons right">add</i>
                </button>
            </a>
        </div>
        <div style="margin-top: 10px">
            <table class="bordered centered">
                <thead>
                <tr>
                    <th>Код поставщика</th>
                    <th>Название</th>
                    <th>Город</th>
                    <th>Телефон</th>
                    <th>Доп. информация</th>
                    <th>Операции</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($supplsList as $supplItem): ?>
                    <tr>
                        <td><?php echo $supplItem['code_s']; ?></td>
                        <td><?php echo $supplItem['name_s']; ?></td>
                        <td><?php echo $supplItem['city_s']; ?></td>
                        <td><?php echo $supplItem['phone_s']; ?></td>
                        <td><?php echo $supplItem['info_s']; ?></td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <a href="suppl/delete/<?php echo $supplItem['code_s']; ?>"><i class="small material-icons">delete</i></a>
                                    </td>
                                    <td>
                                        <a href="suppl/edit/<?php echo $supplItem['code_s']; ?>"><i class="small material-icons">mode_edit</i></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?include ROOT.'/views/layout/footer.php' ?>
