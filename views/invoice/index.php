<? include ROOT.'/views/layout/header.php' ?>

<main>
    <div class="container">
        <div class="center" style="margin-top: 10px">
            <a href="/invoice/add">
                <button class="btn waves-effect waves-light">
                    Добавить поставку<i class="material-icons right">add</i>
                </button>
            </a>
        </div>
        <form action="" method="post" style="margin-top: 10px">
            <select class="browser-default" name="supplInvoice">
                <?php foreach ($supples as $supplItem): ?>
                <option value="<?php echo $supplItem['code_s']; ?>"><?php echo $supplItem['name_s']; ?></option>
                <?php endforeach; ?>
            </select>
            <div class="center" style="margin-top: 10px">
                    <button class="btn waves-effect waves-light" type="submit" name="sendSupplInvoice">
                        Далее
                    </button>
            </div>
        </form>
    </div>
</main>

<?include ROOT.'/views/layout/footer.php' ?>