<? include ROOT.'/views/layout/header.php' ?>

    <main>
        <div class="container">
            <table style="margin-top: 10px" class="bordered centered">
                <thead>
                <tr>
                    <th>Номер</th>
                    <th>Дата</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($invoicesBySuppl as $invoiceItem): ?>
                    <tr>
                        <td><a href="/invoice/detail/<?php echo $invoiceItem['num_i']; ?>"> <?php echo $invoiceItem['num_i']; ?></a></td>
                        <td><?php echo $invoiceItem['date_p']; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </main>

<?include ROOT.'/views/layout/footer.php' ?>