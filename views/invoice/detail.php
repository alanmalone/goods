<? include ROOT.'/views/layout/header.php' ?>

    <main>
        <div class="container">
            <table class="bordered centered">
                <thead>
                <tr>
                    <th>Номер</th>
                    <th>Артикул товара</th>
                    <th>Количество</th>
                    <th>Цена за единицу</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $details['num_i']; ?></td>
                        <td><?php echo $details['art']; ?></td>
                        <td><?php echo $details['qt']; ?></td>
                        <td><?php echo $details['price_p']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </main>

<?include ROOT.'/views/layout/footer.php' ?>