<? include ROOT.'/views/layout/header.php' ?>

    <main>
        <div class="container">
            <form action="" method="post" style="margin-top: 10px">

                <div class="row">
                    <div class="input-field col s12">
                        <input id="numI" name="num_i" type="text" class="validate" maxlength="6">
                        <label for="codeS">Номер поставки</label>
                    </div>
                </div>

                <select class="browser-default" name="addSupplInvoice">
                    <?php foreach ($supples as $supplItem): ?>
                        <option value="<?php echo $supplItem['code_s']; ?>"><?php echo $supplItem['name_s']; ?></option>
                    <?php endforeach; ?>
                </select>

                <div class="row">
                    <div class="input-field col s12">
                        <input id="dateP" name="date_p" type="date" class="datepicker">
                    </div>
                </div>

                <select class="browser-default" name="addGoodInvoice">
                    <?php foreach ($goods as $goodItem): ?>
                        <option value="<?php echo $goodItem['art']; ?>"><?php echo $goodItem['name_g']; ?></option>
                    <?php endforeach; ?>
                </select>

                <div class="row">
                    <div class="input-field col s12">
                        <input id="Qt" name="qt" type="text" class="validate" maxlength="4">
                        <label for="codeS">Количество</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <input id="priceP" name="price_p" type="text" class="validate" maxlength="4">
                        <label for="codeS">Цена за штуку</label>
                    </div>
                </div>

                <div class="center-align" style="margin-top: 10px">
                    <button class="btn waves-effect waves-light" type="submit" name="addInvoice">
                        Добавить<i class="material-icons right">add</i>
                    </button>
                </div>

            </form>
        </div>
    </main>
<?include ROOT.'/views/layout/footer.php' ?>
