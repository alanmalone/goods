<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="/template/css/ghpages-materialize.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
<header>
    <nav class="top-nav">
        <div class="container">
            <div class="nav-wrapper"><a class="page-title"><?php echo $pageName?></a></div>
        </div>
    </nav>
    <div class="container"><a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a></div>
    <ul id="nav-mobile" class="side-nav fixed">
        <li style="height: 128px; margin-bottom: 10px; margin-top: 10px">
            <div class="center-align">
                <a id="logo-container" href="/"><img style="width: 128px; height: 128px;" src="/template/images/logo.jpg"></a>
            </div>
        </li>
        <li class="bold"><a href="/goods" class="waves-effect waves-teal">Товары</a></li>
        <li class="bold"><a href="/suppl" class="waves-effect waves-teal">Поставщики</a></li>
        <li class="bold"><a href="/invoice" class="waves-effect waves-teal">Поставки</a></li>
        <li class="bold"><a href="/report" class="waves-effect waves-teal">Отчеты</a></li>
    </ul>
</header>