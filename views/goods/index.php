<? include ROOT.'/views/layout/header.php' ?>
<main>
    <div class="container">
        <div class="center-align" style="margin-top: 10px">
            <a href="goods/add">
            <button class="btn waves-effect waves-light" type="submit" name="action">
                Добавить<i class="material-icons right">add</i>
            </button>
            </a>
        </div>

        <div style="margin-top: 10px">
            <table class="bordered centered">
                <thead>
                <tr>
                    <th>Артикул</th>
                    <th>Наименование</th>
                    <th>Единица измерения</th>
                    <th>Опции</th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($goodsList as $goodItem): ?>
                <tr>
                    <td><?php echo $goodItem['art']; ?></td>
                    <td><?php echo $goodItem['name_g']; ?></td>
                    <td><?php echo $goodItem['meas_g']; ?></td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <a href="goods/delete/<?php echo $goodItem['art']; ?>"><i class="small material-icons">delete</i></a>
                                </td>
                                <td>
                                    <a href="goods/edit/<?php echo $goodItem['art']; ?>"><i class="small material-icons">mode_edit</i></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?include ROOT.'/views/layout/footer.php' ?>
