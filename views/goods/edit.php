<? include ROOT.'/views/layout/header.php' ?>

    <main>
        <form action="" method="post">
            <div class="container">
                <form class="col s12" style="margin-top: 10px">
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="nameG" name="nameG" type="text" value="<?php echo $currentGood['name_g'];?>" class="validate" maxlength="100">
                            <label for="nameG">Наименование</label>
                        </div>
                    </div>
                    <label>
                        <select class="browser-default" name="measG">
                            <option value="л" <?php if ($currentGood['meas_g'] == 'л') echo 'selected';?> >л</option>
                            <option value="шт" <?php if ($currentGood['meas_g'] == 'шт') echo 'selected';?> >шт</option>
                            <option value="кг" <?php if ($currentGood['meas_g'] == 'кг') echo 'selected';?> >кг</option>
                        </select>
                    </label>
                    <div class="center-align" style="margin-top: 10px">
                        <button class="btn waves-effect waves-light" type="submit" name="editGood">
                            Готово<i class="material-icons right">mode_edit</i>
                        </button>
                    </div>
                </form>
            </div>
        </form>
    </main>

<?include ROOT.'/views/layout/footer.php' ?>