<? include ROOT.'/views/layout/header.php' ?>

<main>
    <form action="" method="post">
    <div class="container">
        <form class="col s12" style="margin-top: 10px">
            <div class="row">
                <div class="input-field col s12">
                    <input id="vendor" name="vendorCode" type="text" class="validate" maxlength="8">
                    <label for="vendor">Артикул</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="nameG" name="nameG" type="text" class="validate" maxlength="100">
                    <label for="nameG">Наименование</label>
                </div>
            </div>
            <label>
                <select class="browser-default" name="measG">
                    <option value="" disabled selected>Выберите единицу измерения</option>
                    <option value="л">л</option>
                    <option value="шт">шт</option>
                    <option value="кг">кг</option>
                </select>
            </label>
            <div class="center-align" style="margin-top: 10px">
                <button class="btn waves-effect waves-light" type="submit" name="addGood">
                    Добавить<i class="material-icons right">add</i>
                </button>
            </div>
        </form>
    </div>
    </form>
</main>

<?include ROOT.'/views/layout/footer.php' ?>
