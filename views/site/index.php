<? include ROOT.'/views/layout/header.php' ?>
<main>
    <div class="container">
        <div class="center-align">
            <p class="flow-text">Это веб-приложение для сдачи лабораторных работ по базам данных.</p>
            <p class="flow-text">В данной лабораторной работе я использовал:</p>
            <p class="flow-text">1. PHP</p>
            <p class="flow-text">2. MySQL</p>
            <p class="flow-text">3. Materialize CSS Framework</p>
        </div>
    </div>
</main>
<?include ROOT.'/views/layout/footer.php' ?>