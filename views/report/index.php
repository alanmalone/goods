<? include ROOT . '/views/layout/header.php' ?>

    <main>
        <div class="container">

            <div class="center-align" style="margin-top: 50px">
                <a href="/report/deliveries">
                    <button class="btn waves-effect waves-light" name="supplierDeliveries">
                        Поставки поставщиков
                    </button>
                </a>
            </div>

            <div class="center-align" style="margin-top: 50px">
                <a href="/report/volumes">
                    <button class="btn waves-effect waves-light" type="submit" name="suppliesVolumes">
                        Обьемы поставок
                    </button>
                </a>
            </div>
        </div>
    </main>

<? include ROOT . '/views/layout/footer.php' ?>