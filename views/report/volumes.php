<? include ROOT . '/views/layout/header.php' ?>

    <main>
        <div class="container">
            <table class="bordered centered">
                <thead>
                <tr>
                    <th>Продукт</th>
                    <th>Поставщик</th>
                    <th>Сумма</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($invoiceGoods as $invoiceItem): ?>
                    <?php $sumGood = 0?>
                    <tr>
                        <td><?php echo $invoiceItem['name_g']; ?></td>
                        <td>
                            <table>
                                <?php foreach ($suppliesByGood = Invoice::getSupplByInvoiceGood($invoiceItem['art']) as $detailItem): ?>
                                    <tr>
                                        <td><?php
                                            echo $detailItem['name_s'];
                                            ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </td>
                        <td>
                            <table>
                                <?php foreach ($suppliesByGood = Invoice::getSupplByInvoiceGood($invoiceItem['art']) as $detailItem): ?>
                                    <tr>
                                        <td><?php
                                            $sumGood += $detailItem['price_p'];
                                            echo $detailItem['price_p'];
                                            ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </td>
                    <tr>
                        <td colspan="5" style="text-align: right"><?php echo $sumGood?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </main>

<? include ROOT . '/views/layout/footer.php' ?>