<? include ROOT . '/views/layout/header.php' ?>

    <main>
        <div class="container">
            <table class="bordered centered">
                <thead>
                <tr>
                    <th>Поставщик</th>
                    <th>Название товара</th>
                    <th>Обьем</th>
                    <th>Цена за единицу</th>
                    <th>Сумма</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($invoiceSupplies as $invoiceItem): ?>
                    <?php $sumSuppl = 0?>
                    <tr>
                        <td><?php echo $invoiceItem['name_s']; ?></td>
                        <td>
                            <table>
                                <?php foreach ($detailsBySuppl = Detail::getDetailBySupplId($invoiceItem['code_s']) as $detailItem): ?>
                                    <tr>
                                        <td><?php
                                            $count = count($detailsBySuppl);
                                            echo $detailItem['name_g'];
                                            ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </td>
                        <td>
                            <table>
                                <?php foreach ($detailsBySuppl = Detail::getDetailBySupplId($invoiceItem['code_s']) as $detailItem): ?>
                                    <tr>
                                        <td><?php echo $detailItem['qt']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </td>
                        <td>
                            <table>
                                <?php foreach ($detailsBySuppl = Detail::getDetailBySupplId($invoiceItem['code_s']) as $detailItem): ?>
                                    <tr>
                                        <td><?php echo $detailItem['price_p']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </td>
                        <td>
                            <table>
                                <?php foreach ($detailsBySuppl = Detail::getDetailBySupplId($invoiceItem['code_s']) as $detailItem): ?>
                                    <tr>
                                        <td><?php
                                            $sumSuppl += $detailItem['price_p'] * $detailItem['qt'];
                                            $sum += $detailItem['price_p'] * $detailItem['qt'];
                                            echo($detailItem['price_p'] * $detailItem['qt']);
                                            ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>Всего для <?php echo $invoiceItem['name_s'] . "(" . $count . " записи)"; ?></td>
                        <td colspan="4" style="text-align: right"><?php echo $sumSuppl?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td style="text-align: left">ИТОГО</td>
                    <td colspan="4" style="text-align: right"><?php echo $sum ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </main>

<? include ROOT . '/views/layout/footer.php' ?>