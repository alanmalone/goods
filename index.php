<?php
/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 14.03.2016
 * Time: 18:32
 */
ini_set('display_errors', 1);
error_reporting(E_ALL);

define('ROOT', dirname(__FILE__));
require_once (ROOT.'/components/Router.php');
require_once (ROOT.'/components/Db.php');

$router = new Router();
$router->run();