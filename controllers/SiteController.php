<?php

/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 10.04.2016
 * Time: 18:54
 */
class SiteController {
    public function actionIndex() {
        $pageName = "Вариант 7. Поставки товаров";
        require_once (ROOT . '/views/site/index.php');
        return true;
    }
}