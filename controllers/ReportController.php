<?php

/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 14.06.2016
 * Time: 10:30
 */

include ROOT.'/models/Invoice.php';
include ROOT.'/models/Detail.php';
include ROOT.'/models/Good.php';

class ReportController {
    public function actionIndex() {
        $pageName = "Отчеты";

        require_once (ROOT . '/views/report/index.php');

        return true;
    }

    public function actionDeliveries() {
        $pageName = "Поставки поставщиков";

        $sum = 0;

        $invoiceSupplies = Invoice::getInvoiceSupplies();

        require_once (ROOT . '/views/report/deliveries.php');

        return true;
    }

    public function actionVolumes() {
        $pageName = "Обьемы поставок";

        $invoiceGoods = Good::getInvoiceGoods();

        require_once (ROOT . '/views/report/volumes.php');

        return true;
    }
}