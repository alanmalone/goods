<?php

/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 10.04.2016
 * Time: 22:45
 */

include ROOT.'/models/Good.php';

class GoodsController {
    public function actionIndex() {

        $pageName = "Товары";

        $goodsList = Good::getAllGoods();

        require_once (ROOT . '/views/goods/index.php');

        return true;
    }

    public function actionAdd() {
        $pageName = "Добавление товара";

        if (isset($_POST['addGood'])) {

            $vendorCode = $_POST['vendorCode'];
            $nameG = $_POST['nameG'];
            $measG = $_POST['measG'];

            Good::addGood($vendorCode, $nameG, $measG);

            header("Location: /goods");
        }

        require_once (ROOT . '/views/goods/add.php');

        return true;
    }

    public function actionDelete($id) {
        Good::deleteGoodById($id);

        header("Location: /goods");

        return true;
    }

    public function actionEdit($id) {

        $pageName = "Редактировать товар";

        $currentGood = Good::getGoodById($id);

        if (isset($_POST['editGood'])) {

            $art = $_POST['vendorCode'];
            $nameG = $_POST['nameG'];
            $measG = $_POST['measG'];

            Good::updateGood($art, $nameG, $measG);

            header("Location: /goods");
        }

        require_once (ROOT . '/views/goods/edit.php');

        return true;
    }
}