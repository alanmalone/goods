<?php

/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 13.06.2016
 * Time: 16:28
 */

include ROOT.'/models/Suppl.php';
include ROOT.'/models/Invoice.php';
include ROOT.'/models/Detail.php';
include ROOT.'/models/Good.php';

class InvoiceController {
    public function actionIndex() {
        $pageName = "Поставки";

        $supples = Suppl::getAllSuppls();

        if (isset($_POST['sendSupplInvoice'])) {
            //echo "123";
            $code_s = $_POST['supplInvoice'];
            header("Location: /invoice/delivery/" . $code_s);
        }

        require_once (ROOT . '/views/invoice/index.php');

        return true;
    }

    public function actionDelivery($code_s) {
        $suppl = Suppl::getSupplById($code_s);

        $pageName = $suppl['name_s'];

        $invoicesBySuppl = Invoice::getInvoiceBySupplId($code_s);

        require_once (ROOT . '/views/invoice/delivery.php');

        return true;
    }

    public function actionDetail($num_i) {
        $details = Detail::getDetailById($num_i);

        $suppl = Suppl::getSupplById($details['code_s']);

        $pageName = $suppl['name_s'];

        require_once (ROOT . '/views/invoice/detail.php');

        return true;
    }

    public function actionAdd() {
        $supples = Suppl::getAllSuppls();

        $goods = Good::getAllGoods();

        $pageName = "Добаление поставки";

        if (isset($_POST['addInvoice'])) {
            $num_i = $_POST['num_i'];
            $code_s = $_POST['addSupplInvoice'];
            $date_p = $_POST['date_p'];
            $art = $_POST['addGoodInvoice'];
            $qt = $_POST['qt'];
            $price_p = $_POST['price_p'];

            Invoice::addInvoice($num_i, $code_s, $date_p, $art, $qt, $price_p);

            header("Location: /invoice");
        }

        require_once (ROOT . '/views/invoice/add.php');

        return true;
    }
}