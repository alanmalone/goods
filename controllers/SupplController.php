<?php

/**
 * Created by PhpStorm.
 * User: AlanMalone
 * Date: 11.05.2016
 * Time: 16:56
 */

include ROOT.'/models/Suppl.php';

class SupplController {
    public function actionIndex() {

        $pageName = "Поставщики";

        $supplsList = Suppl::getAllSuppls();

        require_once (ROOT . '/views/suppl/index.php');

        return true;
    }

    public function actionAdd() {

        $pageName = "Добавление поставщика";

        if (isset($_POST['addSuppl'])) {

            $code_s = $_POST['code_s'];
            $name_s = $_POST['name_s'];
            $city_s = $_POST['city_s'];
            $phone_s = $_POST['phone_s'];
            $stat = $_POST['stat'];
            $info_s = $_POST['info_s'];

            Suppl::addSuppl($code_s, $name_s, $city_s, $phone_s, $stat, $info_s);

            header("Location: /suppl");
        }

        require_once (ROOT . '/views/suppl/add.php');

        return true;
    }

    public function actionDelete($code_s) {
        Suppl::deleteSupplById($code_s);

        header("Location: /goods");

        return true;
    }

    public function actionEdit($code_s) {
        $pageName = "Редактирование поставщика";

        $currentSuppl = Suppl::getSupplById($code_s);

        if (isset($_POST['editSuppl'])) {
            $name_s = $_POST['name_s'];
            $city_s = $_POST['city_s'];
            $phone_s = $_POST['phone_s'];
            $stat = $_POST['stat'];
            $info_s = $_POST['info_s'];

            Suppl::updateSupplById($code_s, $name_s, $city_s, $phone_s, $stat, $info_s);

            header("Location: /suppl");
        }

        require_once (ROOT . '/views/suppl/edit.php');

        return true;
    }
}